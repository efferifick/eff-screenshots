var currentTabId;

browser.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  if (tab.status == 'complete') {
    currentTabId = tabId;
  }
});

function onCaptured(imageUri) {
  browser.tabs.sendMessage(currentTabId, imageUri);
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.browserAction.onClicked.addListener(function() {
  var capturing = browser.tabs.captureVisibleTab();
  capturing.then(onCaptured, onError);
});
